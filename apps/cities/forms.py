from django import forms

from cities.models import City, Region

class CityForm(forms.ModelForm):
    """
    """
    class Meta:
        model = City
        fields = (
            'country',
            'region',
            'name', 
        )
        labels = {
            'name': 'City'
        }
        widgets = {
            'name': forms.Select
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['region'].queryset = Region.objects.none()
        self.fields['name'].queryset = City.objects.none()

        if 'country' in self.data:
            try:
                country_id = int(self.data.get('country'))
                self.fields['region'].queryset = Region.objects.filter(country_id=country_id).order_by('name')
            except (ValueError, TypeError):
                pass # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['region'].queryset = self.instance.country.region_set.order_by('name')