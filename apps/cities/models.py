from django.db import models 
 
from cities_light.abstract_models import AbstractCity, AbstractRegion, AbstractCountry
from cities_light.receivers import connect_default_signals

#import uuid


class Country(AbstractCountry):
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pass
connect_default_signals(Country)

class Region(AbstractRegion):
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pass
connect_default_signals(Region)

class City(AbstractCity):
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    timezone = models.CharField(max_length=40)
connect_default_signals(City)
