from django.shortcuts import render
from django.shortcuts import redirect

from cities.models import Region, City
from cities.forms import CityForm

def HomeView(request):
    """
    """
    context = {}
    return render(request, 'cities/home.html', context) 

def SelectionView(request, city_id):
    """
    """
    city = City.objects.get( pk = city_id)
    print(city)
    context = {'city': city}
    return render(request, 'cities/selection.html', context)     

def SelectView(request):
    """
    """

    if request.method == "POST":
        city_form = CityForm(request.POST)
        if city_form.is_valid():
            print(dir(city_form))
            print(city_form.data)
            return redirect('cities:selection', city_id = city_form.data['name'])
            #context = {'city_id': city_form.data['name']}
            #return render(request, 'cities/selection.html', context)
    else:
        city_form = CityForm()
    context = {
        'form': city_form
    }
    return render(request, 'cities/select.html', context)

# ----------------
# chained dropdwon 

def load_regions(request):
    """
    filter regions for selected country
    """
    country_id = request.GET.get('country')
    regions = Region.objects.filter(country_id=country_id)
    return render(request, 'cities/region_dropdown_list_options.html', {'regions': regions})

def load_cities(request):
    """
    filter cities for selected country/region
    """
    region_id = request.GET.get('region')
    cities = City.objects.filter(region_id=region_id)
    return render(request, 'cities/city_dropdown_list_options.html', {'cities': cities})

# /chained dropdwon
# ----------------