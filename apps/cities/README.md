# manax install info

## edit ../../config/settings/base.py

LOCAL_APPS = (
    [...]
    # Your stuff: custom apps go here
    'cities.apps.CitiesConfig',  
    [...]
)

## edit ../../config/urls.py


    path('cities/', include(('cities.urls', 'cities'), namespace='cities')),