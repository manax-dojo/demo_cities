from django.urls import path

from . import views

urlpatterns = [
    # URL pattern for the HomeView
    path(
        route = '',
        view = views.HomeView,
        name = 'home',
    ),

    path(
        route = 'select/',
        view = views.SelectView,
        name = 'select',
    ),    

    path(
        route = 'selection/<int:city_id>',
        view = views.SelectionView,
        name = 'selection',
    ),    

    # ajax
    path(
        route = 'ajax/load-regions/',
        view = views.load_regions,
        name = 'ajax_load_regions',
    ),
    path(
        route = 'ajax/load-cities/',
        view = views.load_cities,
        name = 'ajax_load_cities',
    ),
]